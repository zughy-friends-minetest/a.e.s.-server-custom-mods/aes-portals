local srcpath = minetest.get_modpath("aes_portals") .. "/src"

aes_portals = {}

dofile(srcpath .. "/core.lua")
dofile(srcpath .. "/commands.lua")
dofile(srcpath .. "/nodes.lua")
dofile(srcpath .. "/utils.lua")

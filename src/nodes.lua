minetest.register_node("aes_portals:portal_bl",{
  description = "Block League portal",
  drawtype = "liquid",
  inventory_image = "aesportals_bl.png^[verticalframe:4:1",
  tiles = {{
    name = "aesportals_bl.png",
    animation = {
      backface_culling = true,
      type = "vertical_frames",
      aspect_w = 16,
      aspect_h = 16,
      length = 0.5,
    },
  }
  },
  liquidtype = "none",
  post_effect_color = {a = 103, r = 40, g = 224, b = 223},
  light_source = 8,
  walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
  drop = "",
})



minetest.register_node("aes_portals:portal_fb",{
  description = "Fantasy Brawl portal",
  drawtype = "liquid",
  inventory_image = "aesportals_fb.png^[verticalframe:7:1",
  tiles = {{
    name = "aesportals_fb.png",
    animation = {
      backface_culling = true,
      type = "vertical_frames",
      aspect_w = 16,
      aspect_h = 16,
      length = 1.25,
    },
  }
  },
  liquidtype = "none",
  post_effect_color = {a = 103, r = 142, g = 71, b = 140},
  light_source = 8,
  walkable = false,
	pointable = false,
	diggable = false,
	buildable_to = true,
  drop = "",
})



minetest.register_node("aes_portals:dequeuer_bl",{
  description = "Fantasy Brawl dequeuer",
  drawtype = "normal",
  inventory_image = "aesportals_bl.png^[verticalframe:4:1^[combine:12x12:0,6=arenalib_editor_quit.png",
  tiles = {{
    name = "aesportals_bl.png",
    animation = {
      backface_culling = true,
      type = "vertical_frames",
      aspect_w = 16,
      aspect_h = 16,
      length = 0.5,
    },
  }
  },
  walkable = true,
	pointable = true,
	diggable = false,
	buildable_to = true,
  drop = "",

  on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
    if clicker:is_player() and arena_lib.is_player_in_queue(clicker:get_player_name(), "block_league") then
      arena_lib.remove_player_from_queue(clicker:get_player_name())
    end
  end,
})



minetest.register_node("aes_portals:dequeuer_fb",{
  description = "Fantasy Brawl dequeuer",
  drawtype = "normal",
  inventory_image = "aesportals_fb.png^[verticalframe:7:1^[combine:12x12:0,6=arenalib_editor_quit.png",
  tiles = {{
    name = "aesportals_fb.png",
    animation = {
      backface_culling = true,
      type = "vertical_frames",
      aspect_w = 16,
      aspect_h = 16,
      length = 1.25,
    },
  }
  },
  walkable = true,
	pointable = true,
	diggable = false,
	buildable_to = true,
  drop = "",

  on_rightclick = function(pos, node, clicker, itemstack, pointed_thing)
    if clicker:is_player() and arena_lib.is_player_in_queue(clicker:get_player_name(), "fantasy_brawl") then
      arena_lib.remove_player_from_queue(clicker:get_player_name())
    end
  end,
})

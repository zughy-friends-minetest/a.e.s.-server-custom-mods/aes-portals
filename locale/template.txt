# textdomain: aes_portals

##[ src/commands.lua ]##
Portals management=
Unknown minigame and/or arena!=
Arena @1 is already linked=
Arena @1 successfully linked=
Arena @1 is already unlinked=
Arena @1 successfully unlinked=
This minigame doesn't exist!=

##[ src/core.lua ]##
Only the party leader can join a game!=
No ongoing arenas found: you've been put into a queue (right-click the portal to leave)=
Arena full: entering as spectator=
There are no arenas available at the moment=

##[ src/sounds.lua ]##
Portals=
Ambient=
Portal echoes=

##[ src/utils.lua ]##
[!] @1=
